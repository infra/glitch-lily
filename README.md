# Glitch-lily
## Fork notice | 复刻通告

[![pipeline status](https://lily.kazv.moe/infra/glitch-lily/badges/servant/pipeline.svg)](https://lily.kazv.moe/infra/glitch-lily/-/commits/servant)
[![coverage report](https://lily.kazv.moe/infra/glitch-lily/badges/servant/coverage.svg)](https://lily.kazv.moe/infra/glitch-lily/-/commits/servant)

This project aims to make Glitch frontend run on top of a Pleroma server just like any other Pleroma client.

这个项目旨在让 Glitch 的前端运行在 Pleroma 服务器高头，就像任何一个别的 Pleroma 客户端一样。

It is not likely that I will add Pleroma-FE-specific features (e.g. image zooming) here.

我大概率不会在这块加 Pleroma-FE 特定的特性（比如讲，图片缩放）。

Note: This repository is regularly kept up with the upstream via a rebase. The last commit time you see on the
home page refers to the time the commit was first *authored*, not pushed. To see the time of the last push,
see the [pipeline page][pipeline-page].

注意：这个仓库常规用 rebase 来跟进上游。你在首页高头看到的最后一次提交的时间，是第一次写那个提交的时间，
不是推送的时间。要看最后一次推送的时间，就看下子 [pipeline 页面][pipeline-page].


[pipeline-page]: https://lily.kazv.moe/infra/glitch-lily/-/pipelines

## Deploying | 部署

Since Soapbox-FE is just a fork of Mastodon-FE, the deployment of Glitch-lily will be very similar to Soapbox-FE.

Soapbox-FE 就是 Mastodon-FE 的一个复刻，所以部署 Glitch-lily 跟 Soapbox-FE 差不多。

Download the built zip archive from [here][artifact] and extract it.

从[这块][artifact]下载 zip 归档，然后解压之。

[artifact]: https://lily.kazv.moe/infra/glitch-lily/-/jobs/artifacts/servant/download?job=build

Create an nginx configuration:

创建一个 nginx 配置：

```
# The host(s) and port(s) of your Pleroma backend.
# 你的 Pleroma 后端的主机跟端口。
upstream pleroma-balancer {
  server 127.0.0.1:4000;
  # ...
}

# Chances are you have already set this up in Pleroma config, so you do not need to repeat this.
# 你大概率已经在 Pleroma 配置里头设好了，那就不用重复搞这个了。
proxy_cache_path /tmp/pleroma-media-cache levels=1:2 keys_zone=pleroma_media_cache:10m max_size=10g
  inactive=720m use_temp_path=off;

# <GlHost> = domain where you want to run Glitch-lily | 你想运行 Glitch-lily 的地方的域名
# <PleromaHost> = domain where you run Pleroma | 你运行 Pleroma 的地方的域名
server {
  listen 80;
  # Comment out the first uncommented line below if you do not have ipv6 or does not want to serve ipv6.
  # 如果你没得 ipv6 或者不想用 ipv6 提供服务，就把下面第一个没得注释掉的一行这个注释掉。
  listen [::]:80;

  server_name <GlHost>;
  location / {
   return 301 https://$server_name$request_uri;
  }
}

server {
  listen 443 ssl;
  # Comment out the first uncommented line below if you do not have ipv6 or does not want to serve ipv6.
  # 如果你没得 ipv6 或者不想用 ipv6 提供服务，就把下面第一个没得注释掉的一行这个注释掉。
  listen [::]:443 ssl;

  server_name <GlHost>;
  ssl_session_timeout 1d;
  ssl_session_cache shared:MozSSL:10m;  # about 40000 sessions
  ssl_session_tickets off;

  # Change the key and cert location as needed.
  # 根据需要改密钥跟证书的位置。
  ssl_trusted_certificate   <PathTo>/chain.pem;
  ssl_certificate <PathTo>/fullchain.pem;
  ssl_certificate_key <PathTo>/privkey.pem;
  ssl_protocols TLSv1.2 TLSv1.3;

  ssl_ciphers "ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4";
    ssl_prefer_server_ciphers off;
    # In case of an old server with an OpenSSL version of 1.0.2 or below,
    # leave only prime256v1 or comment out the following line.
    ssl_ecdh_curve X25519:prime256v1:secp384r1:secp521r1;
    ssl_stapling on;
    ssl_stapling_verify on;

    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_buffers 16 8k;
    gzip_http_version 1.1;
    gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript application/activity+json application/atom+xml;

    # the nginx default is 1m, not enough for large media uploads
    # Should be larger than your upload limit.
    # 应该设得比你的上传限制要大。
    client_max_body_size 20m;

  # Extracting the zip will give you a directory named public . Set this to that path.
  # 解压 zip 会给你一个叫作 public 的目录。把这个设成那个路径。
  root <PathTo>/public/;

  # Do not expose the healthcheck endpoint to the outside. Purely for security reasons. Can ignore this.
  # 不把健康检查终点暴露到外头。完全是安全考虑。可以忽略这个。
  location /api/v1/pleroma/healthcheck {
    return 403;
  }
  # I do not know whether Glitch can see the about page from the frontend, but Soapbox asks for this.
  # Should be fine to comment out this section.
  # 我不晓得 Glitch 能不能从前端看关于页面，但是 Soapbox 需要这个玩意儿。
  # 把这段注释掉应该没得关系。
  location /instance/about/index.html {
    proxy_pass http://pleroma-balancer/static/terms-of-service.html;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_set_header Host <PleromaHost>;
  }

  # The reason for separating out emoji is only because it can get way more requests than others,
  # so you may want to have separate settings like rate-limiting.
  # 把 emoji 分出来的原因只是因为这个可能会比别的地方得到的请求多得多了，
  # 所以你可能想搞点儿分别的设置，比如讲限制频率。
  location /emoji {
    proxy_pass http://pleroma-balancer;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_set_header Host <PleromaHost>;
  }

  location ~ ^/(api|.well-known|nodeinfo|proxy|media|oauth|favicon.*|instance|images) {
    proxy_pass http://pleroma-balancer;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_set_header Host <PleromaHost>;
  }

    # Media proxy. Note we changed the cache key to make it reuse the one on Pleroma backend.
    # 图介。注意到把缓存键改了让它重用 Pleroma 后端高头的那个。
    location ~ ^/(media|proxy) {
        proxy_cache        pleroma_media_cache;
        slice              1m;
        proxy_cache_key    <PleromaHost>$uri$is_args$args$slice_range;
        proxy_set_header   Range $slice_range;
        proxy_http_version 1.1;
        proxy_cache_valid  200 206 301 304 1h;
        proxy_cache_lock   on;
        proxy_ignore_client_abort on;
        proxy_buffering    on;
        chunked_transfer_encoding on;
        proxy_pass         http://pleroma-balancer;
        proxy_set_header Host <PleromaHost>;
    }

    location / {
        try_files $uri /index.html =404;
    }

    location = /index.html {
        expires 30s;
    }

}
```

Reload your nginx config (usually done with `systemctl reload nginx` or `/etc/init.d/nginx reload` or whatever specific to your distribution/init system).

重新载入你的 nginx 配置（通常是用 `systemctl reload nginx` 或者 `/etc/init.d/nginx reload` 或者什么你的发行版/初始化系统特定的东西）。


And you are done. Go to the website and click on "Login" button, and you will be able to login with OAuth on your Pleroma server.

然后就搞好了。走去网站然后点一下「Login」按钮，然后你就能透过你 Pleroma 服务器高头的 OAuth 登录了。

# Upstream README starts here | 上游的说明文档从此开始

# Mastodon Glitch Edition

[![Ruby Testing](https://github.com/glitch-soc/mastodon/actions/workflows/test-ruby.yml/badge.svg)](https://github.com/glitch-soc/mastodon/actions/workflows/test-ruby.yml)
[![Crowdin](https://badges.crowdin.net/glitch-soc/localized.svg)][glitch-crowdin]

[glitch-crowdin]: https://crowdin.com/project/glitch-soc

So here's the deal: we all work on this code, and anyone who uses that does so absolutely at their own risk. can you dig it?

- You can view documentation for this project at [glitch-soc.github.io/docs/](https://glitch-soc.github.io/docs/).
- And contributing guidelines are available [here](CONTRIBUTING.md) and [here](https://glitch-soc.github.io/docs/contributing/).

Mastodon Glitch Edition is a fork of [Mastodon](https://github.com/mastodon/mastodon). Upstream's README file is reproduced below.

---

<h1><picture>
  <source media="(prefers-color-scheme: dark)" srcset="./lib/assets/wordmark.dark.png?raw=true">
  <source media="(prefers-color-scheme: light)" srcset="./lib/assets/wordmark.light.png?raw=true">
  <img alt="Mastodon" src="./lib/assets/wordmark.light.png?raw=true" height="34">
</picture></h1>

[![GitHub release](https://img.shields.io/github/release/mastodon/mastodon.svg)][releases]
[![Ruby Testing](https://github.com/mastodon/mastodon/actions/workflows/test-ruby.yml/badge.svg)](https://github.com/mastodon/mastodon/actions/workflows/test-ruby.yml)
[![Crowdin](https://d322cqt584bo4o.cloudfront.net/mastodon/localized.svg)][crowdin]

[releases]: https://github.com/mastodon/mastodon/releases
[crowdin]: https://crowdin.com/project/mastodon

Mastodon is a **free, open-source social network server** based on ActivityPub where users can follow friends and discover new ones. On Mastodon, users can publish anything they want: links, pictures, text, and video. All Mastodon servers are interoperable as a federated network (users on one server can seamlessly communicate with users from another one, including non-Mastodon software that implements ActivityPub!)

Click below to **learn more** in a video:

[![Screenshot](https://blog.joinmastodon.org/2018/06/why-activitypub-is-the-future/ezgif-2-60f1b00403.gif)][youtube_demo]

[youtube_demo]: https://www.youtube.com/watch?v=IPSbNdBmWKE

## Navigation

- [Project homepage 🐘](https://joinmastodon.org)
- [Support the development via Patreon][patreon]
- [View sponsors](https://joinmastodon.org/sponsors)
- [Blog](https://blog.joinmastodon.org)
- [Documentation](https://docs.joinmastodon.org)
- [Roadmap](https://joinmastodon.org/roadmap)
- [Official Docker image](https://github.com/mastodon/mastodon/pkgs/container/mastodon)
- [Browse Mastodon servers](https://joinmastodon.org/communities)
- [Browse Mastodon apps](https://joinmastodon.org/apps)

[patreon]: https://www.patreon.com/mastodon

## Features

<img src="/app/javascript/images/elephant_ui_working.svg?raw=true" align="right" width="30%" />

### No vendor lock-in: Fully interoperable with any conforming platform

It doesn't have to be Mastodon; whatever implements ActivityPub is part of the social network! [Learn more](https://blog.joinmastodon.org/2018/06/why-activitypub-is-the-future/)

### Real-time, chronological timeline updates

Updates of people you're following appear in real-time in the UI via WebSockets. There's a firehose view as well!

### Media attachments like images and short videos

Upload and view images and WebM/MP4 videos attached to the updates. Videos with no audio track are treated like GIFs; normal videos loop continuously!

### Safety and moderation tools

Mastodon includes private posts, locked accounts, phrase filtering, muting, blocking, and all sorts of other features, along with a reporting and moderation system. [Learn more](https://blog.joinmastodon.org/2018/07/cage-the-mastodon/)

### OAuth2 and a straightforward REST API

Mastodon acts as an OAuth2 provider, so 3rd party apps can use the REST and Streaming APIs. This results in a rich app ecosystem with a lot of choices!

## Deployment

### Tech stack

- **Ruby on Rails** powers the REST API and other web pages
- **React.js** and **Redux** are used for the dynamic parts of the interface
- **Node.js** powers the streaming API

### Requirements

- **PostgreSQL** 12+
- **Redis** 4+
- **Ruby** 3.1+
- **Node.js** 18+

The repository includes deployment configurations for **Docker and docker-compose** as well as specific platforms like **Heroku**, and **Scalingo**. For Helm charts, reference the [mastodon/chart repository](https://github.com/mastodon/chart). The [**standalone** installation guide](https://docs.joinmastodon.org/admin/install/) is available in the documentation.

## Development

### Vagrant

A **Vagrant** configuration is included for development purposes. To use it, complete the following steps:

- Install Vagrant and Virtualbox
- Install the `vagrant-hostsupdater` plugin: `vagrant plugin install vagrant-hostsupdater`
- Run `vagrant up`
- Run `vagrant ssh -c "cd /vagrant && bin/dev"`
- Open `http://mastodon.local` in your browser

### macOS

To set up **macOS** for native development, complete the following steps:

- Install [Homebrew] and run `brew install postgresql@14 redis imagemagick
libidn nvm` to install the required project dependencies
- Use a Ruby version manager to activate the ruby in `.ruby-version` and run
  `nvm use` to activate the node version from `.nvmrc`
- Run the `bin/setup` script, which will install the required ruby gems and node
  packages and prepare the database for local development
- Finally, run the `bin/dev` script which will launch services via `overmind`
  (if installed) or `foreman`

### Docker

For production hosting and deployment with **Docker**, use the `Dockerfile` and
`docker-compose.yml` in the project root directory.

For local development, install and launch [Docker], and run:

```shell
docker compose -f .devcontainer/compose.yaml up -d
docker compose -f .devcontainer/compose.yaml exec app bin/setup
docker compose -f .devcontainer/compose.yaml exec app bin/dev
```

### Dev Containers

Within IDEs that support the [Development Containers] specification, start the
"Mastodon on local machine" container from the editor. The necessary `docker
compose` commands to build and setup the container should run automatically. For
**Visual Studio Code** this requires installing the [Dev Container extension].

### GitHub Codespaces

[GitHub Codespaces] provides a web-based version of VS Code and a cloud hosted
development environment configured with the software needed for this project.

[![Open in GitHub Codespaces](https://github.com/codespaces/badge.svg)][codespace]

- Click the button to create a new codespace, and confirm the options
- Wait for the environment to build (takes a few minutes)
- When the editor is ready, run `bin/dev` in the terminal
- Wait for an _Open in Browser_ prompt. This will open Mastodon
- On the _Ports_ tab "stream" setting change _Port visibility_ → _Public_

## Contributing

Mastodon is **free, open-source software** licensed under **AGPLv3**.

You can open issues for bugs you've found or features you think are missing. You can also submit pull requests to this repository or submit translations using Crowdin. To get started, take a look at [CONTRIBUTING.md](CONTRIBUTING.md). If your contributions are accepted into Mastodon, you can request to be paid through [our OpenCollective](https://opencollective.com/mastodon).

**IRC channel**: #mastodon on irc.libera.chat

## License

Copyright (C) 2016-2024 Eugen Rochko & other Mastodon contributors (see [AUTHORS.md](AUTHORS.md))

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

[codespace]: https://codespaces.new/mastodon/mastodon?quickstart=1&devcontainer_path=.devcontainer%2Fcodespaces%2Fdevcontainer.json
[Dev Container extension]: https://containers.dev/supporting#dev-containers
[Development Containers]: https://containers.dev/supporting
[Docker]: https://docs.docker.com
[GitHub Codespaces]: https://docs.github.com/en/codespaces
[Homebrew]: https://brew.sh
