
import React, { useCallback, useContext, useState, useMemo } from 'react';
import { defineMessages, FormattedMessage, injectIntl } from 'react-intl';
import { localeNames, LangSettingsContext } from 'lily/utils/locale';

const getLangName = (code) => {
  try {
    return (new Intl.DisplayNames(code, { type: 'language' })).of(code);
  } catch (_) {
    return code;
  }
};

const LanguageSettings = (props) => {
  const { locale, setLocale } = useContext(LangSettingsContext);
  const items = localeNames;

  const localeToDisplayName = useMemo(() => {
    const map = {};
    items.forEach(code => map[code] = getLangName(code));
    return map
  }, [items]);

  const [settingsLocale, setSettingsLocale] = useState(locale);

  const handleChange = useCallback((e) => {
    setSettingsLocale(e.target.value);
  }, [setSettingsLocale]);

  const handleApply = useCallback(() => {
    // Locale settings only take effect on next load
    setLocale(settingsLocale)
      .then(() => window.location.reload());
  }, [setLocale, settingsLocale]);


  return (
    <div>
      <select
        value={settingsLocale}
        onChange={handleChange}
      >
        { items.map(i => <option key={i} value={i}>{localeToDisplayName[i]}</option>) }
      </select>
      <button onClick={handleApply}>
        { /* XXX: Not really the correct one, but the closest I can find from the messages. */ }
        <FormattedMessage id='upload_modal.apply' defaultMessage='Apply' />
      </button>
    </div>
  );
};

export default injectIntl(LanguageSettings);
