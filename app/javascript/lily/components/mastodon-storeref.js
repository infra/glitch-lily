
import React, { useEffect } from 'react';
import Mastodon from 'flavours/glitch/containers/mastodon';
import { store } from 'flavours/glitch/store';

const MastodonStoreRef = (props) => {
  const { storeRef, childProps } = props;
  useEffect(() => {
    storeRef(store);
  }, [storeRef]);
  return <Mastodon {...childProps} />;
};

export default MastodonStoreRef;
