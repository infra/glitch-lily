#!/bin/bash

url=https://www.iana.org/assignments/language-subtag-registry/language-subtag-registry
wget -Olanguage-tags-cache "$url" || exit 1

{
  echo 'const languages = {'
  type=
  langDesc=
  langCode=

  while read line; do
    if [[ "$line" == 'Type: '* ]]; then
      if [[ "$line" == 'Type: language' ]]; then
        type=language
      else
        type=notlanguage
      fi

      if [[ "$langCode" ]]; then
        # Mastodon expects this to be [key, value[0], value[1]]
        # https://github.com/mastodon/mastodon/pull/18420/files#diff-cf40a3c41d4a0b9d651510f966dda50c99f8bdce522cfa3f7411cac807bcd9d4R83
        echo "  '$langCode': \"$langDesc\","
      fi
      langDesc=
      langCode=

      continue
    fi

    if [[ "$type" != 'language' ]]; then
      continue
    fi

    if [[ "$line" == 'Subtag: '* ]]; then
      langCode="${line#Subtag: }"
    elif [[ "$line" == 'Description: '* ]]; then
      langDesc="${line#Description: }"
    fi
  done < language-tags-cache

  echo '};'
  echo
  echo 'export default languages;'
} > app/javascript/lily/utils/language-tags.js
