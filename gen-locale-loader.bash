#!/bin/bash

scriptDir="$(dirname "$(realpath "$0")")"

cd "$scriptDir"

pushd app/javascript/flavours/glitch/locales/
locales=(*)
popd

loaderFile='app/javascript/lily/utils/locale-loader.js'

{
  echo 'const loader = {'
  for f in "${locales[@]}"; do
    lang="${f%.json}"
    if [ "$lang" == "$f" ]; then
      continue
    fi
    # Upstream will take care of loading the locale, we just make a map so
    # we know which ones are available.
    echo "  '$lang': true,"
  done
  echo '};'
  echo
  echo 'export { loader };'
  echo
} > "$loaderFile"
